import React from 'react';
import ReactDOM from 'react-dom';

import Header from '../src/Header';
import Link from '../src/Link';
import Menu from '../src/Menu';
import '../src/Header/Header.scss';
import '../src/Menu/Menu.scss';
// import Video from '../src/Video';

ReactDOM.render(
  <div>
    <h1>Example Components</h1>
    <Header />
    <Link>Link test</Link>
    <Menu
      items={[
        { name: 'Test', url: '/' },
      ]}
    />
    {/* <Video
      videoId="dQw4w9WgXcQ"
    /> */}
  </div>,
  document.getElementById('app')
);
