import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link as ReactLink } from 'react-router';

class Link extends Component {

  static propTypes = {
    to: PropTypes.string,
    ariaLabel: PropTypes.string,
    onlyActiveOnIndex: PropTypes.bool,
  }

  static defaultProps = {
    to: '/',
  }

  parseTo(to) {
    let parser = document.createElement('a');
    parser.href = to;
    return parser;
  }

  isInternal(to) {
    // If it's a relative url such as '/path', 'path' and does not contain a protocol we can assume it is internal.

    if (to.indexOf("://") === -1) return true;

    const toLocation = this.parseTo(to);
    return window.location.hostname === toLocation.hostname;
  }

  render() {
    const {to, children, ...rest} = this.props;
    const isInternal = this.isInternal(to);

    if (isInternal) {
      return (
        <ReactLink
          onlyActiveOnIndex={this.props.onlyActiveOnIndex}
          aria-label={this.props.ariaLabel}
          to={to}
          {...rest}
        >
          {children}
        </ReactLink>
      );
    } else {
      return (
        <a
          href={to}
          aria-label={this.props.ariaLabel}
          {...rest}
        >
          {children}
        </a>
      );
    }
  }
}

export default Link;
