import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './Menu.scss';
import SubMenu from './Menu';
import Link from '../Link';
import { MenuItemsPropTypes } from '../propTypes';

class Menu extends Component {

	static propTypes = {
		items: MenuItemsPropTypes,
		className: PropTypes.string,
		itemClassName: PropTypes.string,
		isHidden: PropTypes.bool,
	};

	constructor(props) {
		super(props);
	}

	render() {
		return (
			<ul className={classNames(this.props.className, 'menu')} aria-hidden={this.props.isHidden}>
				{this.props.items && this.props.items.map((item, i) => {

					return (
						<li className={classNames(this.props.itemClassName, 'menu__item')} key={`MenuItem${i}`}>
							{
								// Check if url is external because React Router Link don't like it.
								(item.url.indexOf('http://') === 0) || (item.url.indexOf('https://') === 0) || (item.isHtmlLink)
									? (
										<a
											href={item.url}
											className={item.isActive ? 'is-active' : null}
											aria-label={item.ariaLabel}
										>{item.name}</a>
									)
									: (
										<Link
											to={item.url}
											activeClassName="is-active"
											className={item.isActive ? 'is-active' : null}
											aria-label={item.ariaLabel}
											onlyActiveOnIndex
										>
											{item.name}
										</Link>
									)
							}
							{
								item.subMenuItems && (
									<SubMenu
										items={item.subMenuItems}
									/>
								)
							}
						</li>
					)

				})}
			</ul>


		);
	}

}

export default Menu;
