/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
// import withStyles from 'isomorphic-style-loader/lib/withStyles';
import classNames from 'classnames';
import { MenuItemsPropTypes } from '../propTypes';
import Menu from '../Menu';

import styles from './Footer.scss';

// @withStyles(styles)
class Footer extends Component {

	static propTypes = {
		menus: PropTypes.arrayOf(PropTypes.shape({
			title: PropTypes.string,
			titleUrl: PropTypes.string,
			items: MenuItemsPropTypes,
		})),
		legalMenuItems: MenuItemsPropTypes,
		isInHeader: PropTypes.bool,
	};

	constructor(props) {
		super(props);
	}

	render() {
		return (
			<footer className={classNames('footer', {
				'footer--is-in-header': this.props.isInHeader,
			})}>
				<div className="container container--md">

					{this.props.menus && (
						<div className="footer__main">
							{this.props.menus.map((menu, i) => {
								return (
									<div className="footer__section" key={`menu-${menu.title}-${i}`}>
										<h3>{menu.title}</h3>
										<Menu
											className="footer__menu"
											itemClassName="footer__menu__item"
											items={menu.items}
										/>
									</div>
								)
							})}
						</div>
					)}

					<div className="footer__base">
						{this.props.socialMenuItems && (
							<Menu
								className="social-menu"
								itemClassName="social-menu__item"
								items={this.props.socialMenuItems}
							/>
						)}

						{this.props.legalMenuItems && (
							<Menu
								className="legal-menu"
								itemClassName="legal-menu__item"
								items={this.props.legalMenuItems}
							/>
						)}
					</div>

				</div>
   		</footer>
		);
	}

}

export default Footer;
