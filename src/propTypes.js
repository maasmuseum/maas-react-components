import PropTypes from 'prop-types';

// Menu items should have this shape
export const MenuItemPropTypes = PropTypes.shape({
  name: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ]).isRequired,
  url: PropTypes.string.isRequired,
  isHtmlLink: PropTypes.bool,
  isActive: PropTypes.bool,
  ariaLabel: PropTypes.string,
});

// An array of MenuItems
export const MenuItemsPropTypes = PropTypes.arrayOf(MenuItemPropTypes);

// IDs can come from Emu (number) or MongoId, so need to account for it
export const IdPropTypes = PropTypes.oneOfType([
  PropTypes.string,
  PropTypes.number,
]);
