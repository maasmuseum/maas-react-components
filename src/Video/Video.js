/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import React, { PropTypes, Component } from 'react';
// import withStyles from 'isomorphic-style-loader/lib/withStyles';
import classNames from 'classnames';
import YoutubeEmbedVideo from 'youtube-embed-video';
import styles from './Video.scss'; // eslint-disable-line no-unused-vars

// @withStyles(styles)
class Video extends Component {

	// See https://github.com/Tiendq/youtube-embed-video
	// Also https://developers.google.com/youtube/player_parameters

	static propTypes = {
		videoId: PropTypes.string,
		autoplay: PropTypes.bool,
		suggestions: PropTypes.bool,
		controls: PropTypes.bool,
		showInfo: PropTypes.bool,
		enhancedPrivacy: PropTypes.bool,
		width: PropTypes.string,
		height: PropTypes.string,
		className: PropTypes.string,
	};

	static defaultProps = {
		autoplay: false,
		suggestions: false,
		controls: true,
		showInfo: false,
		enhancedPrivacy: false,
		width: undefined,
		height: undefined,
	};

	// constructor(props) {
	// 	super(props);
	// }

	render() {
		return (

			<div className={classNames("video", this.props.className)}>
				<YoutubeEmbedVideo
					// videoId={this.props.videoId + '#t=2m30s'}
					videoId={this.props.videoId}
					autoplay={this.props.autoplay}
					suggestions={this.props.suggestions}
					controls={this.props.controls}
					showInfo={this.props.showInfo}
					enhancedPrivacy={this.props.enhancedPrivacy}
					width={this.props.width}
					height={this.props.height}
				/>
			</div>

		);
	}

}

export default Video;
