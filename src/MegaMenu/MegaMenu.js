import React, { Component } from 'react';
import PropTypes from 'prop-types';
// import withStyles from 'isomorphic-style-loader/lib/withStyles';
import classNames from 'classnames';

import styles from './MegaMenu.scss';
import { MenuItemsPropTypes } from '../propTypes';
import Link from '../Link';
import Menu from '../Menu';
import Footer from '../Footer';

// @withStyles(styles)
class MegaMenu extends Component {

	static propTypes = {
		globalItems: MenuItemsPropTypes,
		menuItems: MenuItemsPropTypes,
		onCloseClick: PropTypes.func,
		footerMenus: PropTypes.array,
		socialMenuItems: MenuItemsPropTypes,
		legalMenuItems: MenuItemsPropTypes,
	};

	static defaultProps = {
		isActive: false,
	}

	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div className={classNames("mega-menu", {
				'is-active': this.props.isActive,
			})}>

				{/* <div className="mega-menu__header">
					<a onClick={this.props.onCloseClick} className="mega-menu__close">
	    			<i className="fa fa-close" />
	    		</a>
    		</div> */}

				<div className="mega-menu__body">

					{this.props.menuItems && (
						<Menu
							className="mega-menu__menu-items"
							itemClassName="mega-menu__menu-item"
							items={this.props.menuItems}
						/>
					)}

					<Menu
						className={classNames('mega-menu__global-items', {
							'mega-menu__global-items--sm': this.props.menuItems ? true : false,
						})}
						itemClassName="mega-menu__global-item"
						items={this.props.globalItems}
					/>

					<Footer
						isInHeader={true}
	          menus={this.props.footerMenus}
	          socialMenuItems={this.props.socialMenuItems}
	          legalMenuItems={this.props.legalMenuItems}
	        />

    		</div>

   		</div>
		);
	}

}

export default MegaMenu;
