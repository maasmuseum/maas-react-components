import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { MenuItemsPropTypes } from '../propTypes';
import styles from './Header.scss';
import logo from './logo-maas.gif';
import logoText from './logo-text.png';
import Menu from '../Menu';
import Link from '../Link';
import MegaMenu from '../MegaMenu';

class Header extends Component {

  static propTypes = {
    routing: PropTypes.object,
    title: PropTypes.string,
    titleUrl: PropTypes.string,
    globalMenuItems: MenuItemsPropTypes,
    venueMenuItems: MenuItemsPropTypes,
    primaryMenuItems: MenuItemsPropTypes,
    primaryTitle: PropTypes.string,
    footerMenus: PropTypes.array,
		socialMenuItems: MenuItemsPropTypes,
		legalMenuItems: MenuItemsPropTypes,
    megaMenuMenuItems: MenuItemsPropTypes,
  }

  constructor(props) {
    super(props);

    this.state = {
      isMegaMenuActive: false,
      locationKey: null,
    }
  }

  componentWillReceiveProps(nextProps) {
    const nextLocationKey = nextProps.routing.locationBeforeTransitions.key;

    if (nextLocationKey !== this.state.locationKey) {
      if (this.state.isMegaMenuActive) {
        document.body.classList.remove('no-scroll-mobile');
      }
      // Ensure that menu returns to closed position after new page transition
      this.setState({ locationKey: nextLocationKey, isMegaMenuActive: false });
    }
  }

  toggleMegaMenu = () => {
    this.setState(
      {
        isMegaMenuActive: !this.state.isMegaMenuActive,
      },
      () => {
        if (this.state.isMegaMenuActive) {
          document.body.classList.add('no-scroll-mobile');
        } else {
          document.body.classList.remove('no-scroll-mobile');
        }
      }
    );
  }

  render() {
    return (
      <header className="header">


        <div className="header__global">

          <div className="header__container">

            <div className="header__global__logos">
              <a href="https://maas.museum" className="header__global__logo">
                <img className="header__global__logo-image" src={logo} alt="MAAS main website"/>
                <img className="header__global__logo-text" src={logoText} aria-hidden="true" />
              </a>

              {/* Make this a prop! */}
              <a href="http://www.planning.nsw.gov.au" className="header__global__logo header__global__logo--nsw-pe">
                <img className="header__global__logo-image" src="/assets/images/logo-nsw-pe.png" alt="NSW Planning and Environment homepage" />
              </a>
            </div>

            <Menu
              className="header__global__menu"
              itemClassName="header__global__menu__item"
              items={this.props.globalMenuItems}
              isHidden={true}
            />

            <h1 className="header__global__title"><Link to={this.props.titleUrl}>{this.props.title}</Link></h1>

            <div className="header__icons">
              <button role="button" aria-expanded={this.state.isMegaMenuActive} aria-label="Toggle full menu" onClick={this.toggleMegaMenu} className="header__icon header__icon--menu">
                <i
                  className={classNames('fa', {
                    'fa-bars': !this.state.isMegaMenuActive,
                    'fa-close': this.state.isMegaMenuActive
                  })}
                />
              </button>

              {/* {this.props.isAdmin && (
                <Link to="/" className="header__icon header__icon--user"><i className="fa fa-user" /></Link>
              )} */}

              {/* <Link to="/" className="header__icon header__icon--shopping-cart"><i className="fa fa-shopping-cart" /></Link> */}
              {/* <Link to="/search" className="header__icon header__icon--search"><i className="fa fa-search" /></Link> */}
            </div>

          </div>

        </div>

        {this.props.venueMenuItems && (
          <Menu
            className={classNames('header__venue-menu', {
              'header__venue-menu--is-active': this.state.isMegaMenuActive
            })}
            itemClassName="header__venue-menu__item"
            items={this.props.venueMenuItems}
            isHidden={!this.state.isMegaMenuActive}
          />
        )}

        <div className="header__primary">

          <div className="header__primary__line"></div>

          <div className="container container--lg">
            <div className="header__primary__holder">
              <Menu
                className="header__primary__menu"
                itemClassName="header__primary__menu__item"
                items={this.props.primaryMenuItems}
                isHidden={false}
              />
            </div>
          </div>

          <MegaMenu
            globalItems={this.props.globalMenuItems}
            menuItems={this.props.megaMenuMenuItems}
            footerMenus={this.props.footerMenus}
            socialMenuItems={this.props.socialMenuItems}
            legalMenuItems={this.props.legalMenuItems}
            isActive={this.state.isMegaMenuActive}
            onCloseClick={this.toggleMegaMenu}
          />

        </div>

      </header>
    )
  }

}

export default Header;
