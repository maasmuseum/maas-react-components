(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("react"), require("prop-types"), require("classnames"), require("react-router"));
	else if(typeof define === 'function' && define.amd)
		define(["React", "prop-types", "classnames", "react-router"], factory);
	else if(typeof exports === 'object')
		exports["maas-react-components"] = factory(require("react"), require("prop-types"), require("classnames"), require("react-router"));
	else
		root["maas-react-components"] = factory(root["React"], root["PropTypes"], root["classNames"], root["ReactRouter"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_3__, __WEBPACK_EXTERNAL_MODULE_4__, __WEBPACK_EXTERNAL_MODULE_5__, __WEBPACK_EXTERNAL_MODULE_13__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(1);


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(process) {'use strict';
	
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _react = __webpack_require__(3);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _propTypes = __webpack_require__(4);
	
	var _propTypes2 = _interopRequireDefault(_propTypes);
	
	var _classnames = __webpack_require__(5);
	
	var _classnames2 = _interopRequireDefault(_classnames);
	
	var _propTypes3 = __webpack_require__(6);
	
	var _Menu = __webpack_require__(7);
	
	var _Menu2 = _interopRequireDefault(_Menu);
	
	var _Footer = __webpack_require__(14);
	
	var _Footer2 = _interopRequireDefault(_Footer);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */
	
	// import withStyles from 'isomorphic-style-loader/lib/withStyles';
	
	
	// @withStyles(styles)
	var Footer = function (_Component) {
		_inherits(Footer, _Component);
	
		function Footer(props) {
			_classCallCheck(this, Footer);
	
			return _possibleConstructorReturn(this, (Footer.__proto__ || Object.getPrototypeOf(Footer)).call(this, props));
		}
	
		_createClass(Footer, [{
			key: 'render',
			value: function render() {
				return _react2.default.createElement(
					'footer',
					{ className: (0, _classnames2.default)('footer', {
							'footer--is-in-header': this.props.isInHeader
						}) },
					_react2.default.createElement(
						'div',
						{ className: 'container container--md' },
						this.props.menus && _react2.default.createElement(
							'div',
							{ className: 'footer__main' },
							this.props.menus.map(function (menu, i) {
								return _react2.default.createElement(
									'div',
									{ className: 'footer__section', key: 'menu-' + menu.title + '-' + i },
									_react2.default.createElement(
										'h3',
										null,
										menu.title
									),
									_react2.default.createElement(_Menu2.default, {
										className: 'footer__menu',
										itemClassName: 'footer__menu__item',
										items: menu.items
									})
								);
							})
						),
						_react2.default.createElement(
							'div',
							{ className: 'footer__base' },
							this.props.socialMenuItems && _react2.default.createElement(_Menu2.default, {
								className: 'social-menu',
								itemClassName: 'social-menu__item',
								items: this.props.socialMenuItems
							}),
							this.props.legalMenuItems && _react2.default.createElement(_Menu2.default, {
								className: 'legal-menu',
								itemClassName: 'legal-menu__item',
								items: this.props.legalMenuItems
							})
						)
					)
				);
			}
		}]);
	
		return Footer;
	}(_react.Component);
	
	process.env.NODE_ENV !== "production" ? Footer.propTypes = {
		menus: _propTypes2.default.arrayOf(_propTypes2.default.shape({
			title: _propTypes2.default.string,
			titleUrl: _propTypes2.default.string,
			items: _propTypes3.MenuItemsPropTypes
		})),
		legalMenuItems: _propTypes3.MenuItemsPropTypes,
		isInHeader: _propTypes2.default.bool
	} : void 0;
	exports.default = Footer;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),
/* 2 */
/***/ (function(module, exports) {

	// shim for using process in browser
	var process = module.exports = {};
	
	// cached from whatever global is present so that test runners that stub it
	// don't break things.  But we need to wrap it in a try catch in case it is
	// wrapped in strict mode code which doesn't define any globals.  It's inside a
	// function because try/catches deoptimize in certain engines.
	
	var cachedSetTimeout;
	var cachedClearTimeout;
	
	function defaultSetTimout() {
	    throw new Error('setTimeout has not been defined');
	}
	function defaultClearTimeout () {
	    throw new Error('clearTimeout has not been defined');
	}
	(function () {
	    try {
	        if (typeof setTimeout === 'function') {
	            cachedSetTimeout = setTimeout;
	        } else {
	            cachedSetTimeout = defaultSetTimout;
	        }
	    } catch (e) {
	        cachedSetTimeout = defaultSetTimout;
	    }
	    try {
	        if (typeof clearTimeout === 'function') {
	            cachedClearTimeout = clearTimeout;
	        } else {
	            cachedClearTimeout = defaultClearTimeout;
	        }
	    } catch (e) {
	        cachedClearTimeout = defaultClearTimeout;
	    }
	} ())
	function runTimeout(fun) {
	    if (cachedSetTimeout === setTimeout) {
	        //normal enviroments in sane situations
	        return setTimeout(fun, 0);
	    }
	    // if setTimeout wasn't available but was latter defined
	    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
	        cachedSetTimeout = setTimeout;
	        return setTimeout(fun, 0);
	    }
	    try {
	        // when when somebody has screwed with setTimeout but no I.E. maddness
	        return cachedSetTimeout(fun, 0);
	    } catch(e){
	        try {
	            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
	            return cachedSetTimeout.call(null, fun, 0);
	        } catch(e){
	            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
	            return cachedSetTimeout.call(this, fun, 0);
	        }
	    }
	
	
	}
	function runClearTimeout(marker) {
	    if (cachedClearTimeout === clearTimeout) {
	        //normal enviroments in sane situations
	        return clearTimeout(marker);
	    }
	    // if clearTimeout wasn't available but was latter defined
	    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
	        cachedClearTimeout = clearTimeout;
	        return clearTimeout(marker);
	    }
	    try {
	        // when when somebody has screwed with setTimeout but no I.E. maddness
	        return cachedClearTimeout(marker);
	    } catch (e){
	        try {
	            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
	            return cachedClearTimeout.call(null, marker);
	        } catch (e){
	            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
	            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
	            return cachedClearTimeout.call(this, marker);
	        }
	    }
	
	
	
	}
	var queue = [];
	var draining = false;
	var currentQueue;
	var queueIndex = -1;
	
	function cleanUpNextTick() {
	    if (!draining || !currentQueue) {
	        return;
	    }
	    draining = false;
	    if (currentQueue.length) {
	        queue = currentQueue.concat(queue);
	    } else {
	        queueIndex = -1;
	    }
	    if (queue.length) {
	        drainQueue();
	    }
	}
	
	function drainQueue() {
	    if (draining) {
	        return;
	    }
	    var timeout = runTimeout(cleanUpNextTick);
	    draining = true;
	
	    var len = queue.length;
	    while(len) {
	        currentQueue = queue;
	        queue = [];
	        while (++queueIndex < len) {
	            if (currentQueue) {
	                currentQueue[queueIndex].run();
	            }
	        }
	        queueIndex = -1;
	        len = queue.length;
	    }
	    currentQueue = null;
	    draining = false;
	    runClearTimeout(timeout);
	}
	
	process.nextTick = function (fun) {
	    var args = new Array(arguments.length - 1);
	    if (arguments.length > 1) {
	        for (var i = 1; i < arguments.length; i++) {
	            args[i - 1] = arguments[i];
	        }
	    }
	    queue.push(new Item(fun, args));
	    if (queue.length === 1 && !draining) {
	        runTimeout(drainQueue);
	    }
	};
	
	// v8 likes predictible objects
	function Item(fun, array) {
	    this.fun = fun;
	    this.array = array;
	}
	Item.prototype.run = function () {
	    this.fun.apply(null, this.array);
	};
	process.title = 'browser';
	process.browser = true;
	process.env = {};
	process.argv = [];
	process.version = ''; // empty string to avoid regexp issues
	process.versions = {};
	
	function noop() {}
	
	process.on = noop;
	process.addListener = noop;
	process.once = noop;
	process.off = noop;
	process.removeListener = noop;
	process.removeAllListeners = noop;
	process.emit = noop;
	process.prependListener = noop;
	process.prependOnceListener = noop;
	
	process.listeners = function (name) { return [] }
	
	process.binding = function (name) {
	    throw new Error('process.binding is not supported');
	};
	
	process.cwd = function () { return '/' };
	process.chdir = function (dir) {
	    throw new Error('process.chdir is not supported');
	};
	process.umask = function() { return 0; };


/***/ }),
/* 3 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_3__;

/***/ }),
/* 4 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_4__;

/***/ }),
/* 5 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_5__;

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.IdPropTypes = exports.MenuItemsPropTypes = exports.MenuItemPropTypes = undefined;
	
	var _propTypes = __webpack_require__(4);
	
	var _propTypes2 = _interopRequireDefault(_propTypes);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	// Menu items should have this shape
	var MenuItemPropTypes = exports.MenuItemPropTypes = _propTypes2.default.shape({
	  name: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.object]).isRequired,
	  url: _propTypes2.default.string.isRequired,
	  isHtmlLink: _propTypes2.default.bool,
	  isActive: _propTypes2.default.bool,
	  ariaLabel: _propTypes2.default.string
	});
	
	// An array of MenuItems
	var MenuItemsPropTypes = exports.MenuItemsPropTypes = _propTypes2.default.arrayOf(MenuItemPropTypes);
	
	// IDs can come from Emu (number) or MongoId, so need to account for it
	var IdPropTypes = exports.IdPropTypes = _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]);

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(process) {'use strict';
	
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _react = __webpack_require__(3);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _propTypes = __webpack_require__(4);
	
	var _propTypes2 = _interopRequireDefault(_propTypes);
	
	var _classnames = __webpack_require__(5);
	
	var _classnames2 = _interopRequireDefault(_classnames);
	
	__webpack_require__(8);
	
	var _Menu = __webpack_require__(7);
	
	var _Menu2 = _interopRequireDefault(_Menu);
	
	var _Link = __webpack_require__(12);
	
	var _Link2 = _interopRequireDefault(_Link);
	
	var _propTypes3 = __webpack_require__(6);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var Menu = function (_Component) {
		_inherits(Menu, _Component);
	
		function Menu(props) {
			_classCallCheck(this, Menu);
	
			return _possibleConstructorReturn(this, (Menu.__proto__ || Object.getPrototypeOf(Menu)).call(this, props));
		}
	
		_createClass(Menu, [{
			key: 'render',
			value: function render() {
				var _this2 = this;
	
				return _react2.default.createElement(
					'ul',
					{ className: (0, _classnames2.default)(this.props.className, 'menu'), 'aria-hidden': this.props.isHidden },
					this.props.items && this.props.items.map(function (item, i) {
	
						return _react2.default.createElement(
							'li',
							{ className: (0, _classnames2.default)(_this2.props.itemClassName, 'menu__item'), key: 'MenuItem' + i },
	
							// Check if url is external because React Router Link don't like it.
							item.url.indexOf('http://') === 0 || item.url.indexOf('https://') === 0 || item.isHtmlLink ? _react2.default.createElement(
								'a',
								{
									href: item.url,
									className: item.isActive ? 'is-active' : null,
									'aria-label': item.ariaLabel
								},
								item.name
							) : _react2.default.createElement(
								_Link2.default,
								{
									to: item.url,
									activeClassName: 'is-active',
									className: item.isActive ? 'is-active' : null,
									'aria-label': item.ariaLabel,
									onlyActiveOnIndex: true
								},
								item.name
							),
							item.subMenuItems && _react2.default.createElement(_Menu2.default, {
								items: item.subMenuItems
							})
						);
					})
				);
			}
		}]);
	
		return Menu;
	}(_react.Component);
	
	process.env.NODE_ENV !== "production" ? Menu.propTypes = {
		items: _propTypes3.MenuItemsPropTypes,
		className: _propTypes2.default.string,
		itemClassName: _propTypes2.default.string,
		isHidden: _propTypes2.default.bool
	} : void 0;
	exports.default = Menu;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),
/* 8 */
/***/ (function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ }),
/* 9 */,
/* 10 */,
/* 11 */,
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(process) {'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _react = __webpack_require__(3);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _propTypes = __webpack_require__(4);
	
	var _propTypes2 = _interopRequireDefault(_propTypes);
	
	var _reactRouter = __webpack_require__(13);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var Link = function (_Component) {
	  _inherits(Link, _Component);
	
	  function Link() {
	    _classCallCheck(this, Link);
	
	    return _possibleConstructorReturn(this, (Link.__proto__ || Object.getPrototypeOf(Link)).apply(this, arguments));
	  }
	
	  _createClass(Link, [{
	    key: 'parseTo',
	    value: function parseTo(to) {
	      var parser = document.createElement('a');
	      parser.href = to;
	      return parser;
	    }
	  }, {
	    key: 'isInternal',
	    value: function isInternal(to) {
	      // If it's a relative url such as '/path', 'path' and does not contain a protocol we can assume it is internal.
	
	      if (to.indexOf("://") === -1) return true;
	
	      var toLocation = this.parseTo(to);
	      return window.location.hostname === toLocation.hostname;
	    }
	  }, {
	    key: 'render',
	    value: function render() {
	      var _props = this.props,
	          to = _props.to,
	          children = _props.children,
	          rest = _objectWithoutProperties(_props, ['to', 'children']);
	
	      var isInternal = this.isInternal(to);
	
	      if (isInternal) {
	        return _react2.default.createElement(
	          _reactRouter.Link,
	          _extends({
	            onlyActiveOnIndex: this.props.onlyActiveOnIndex,
	            'aria-label': this.props.ariaLabel,
	            to: to
	          }, rest),
	          children
	        );
	      } else {
	        return _react2.default.createElement(
	          'a',
	          _extends({
	            href: to,
	            'aria-label': this.props.ariaLabel
	          }, rest),
	          children
	        );
	      }
	    }
	  }]);
	
	  return Link;
	}(_react.Component);
	
	Link.defaultProps = {
	  to: '/'
	};
	process.env.NODE_ENV !== "production" ? Link.propTypes = {
	  to: _propTypes2.default.string,
	  ariaLabel: _propTypes2.default.string,
	  onlyActiveOnIndex: _propTypes2.default.bool
	} : void 0;
	exports.default = Link;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),
/* 13 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_13__;

/***/ }),
/* 14 */
/***/ (function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ })
/******/ ])
});
;
//# sourceMappingURL=Footer.js.map