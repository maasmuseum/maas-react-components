(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("react"), require("prop-types"), require("classnames"));
	else if(typeof define === 'function' && define.amd)
		define(["React", "prop-types", "classnames"], factory);
	else if(typeof exports === 'object')
		exports["maas-react-components"] = factory(require("react"), require("prop-types"), require("classnames"));
	else
		root["maas-react-components"] = factory(root["React"], root["PropTypes"], root["classNames"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_3__, __WEBPACK_EXTERNAL_MODULE_4__, __WEBPACK_EXTERNAL_MODULE_5__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(24);


/***/ }),

/***/ 2:
/***/ (function(module, exports) {

	// shim for using process in browser
	var process = module.exports = {};
	
	// cached from whatever global is present so that test runners that stub it
	// don't break things.  But we need to wrap it in a try catch in case it is
	// wrapped in strict mode code which doesn't define any globals.  It's inside a
	// function because try/catches deoptimize in certain engines.
	
	var cachedSetTimeout;
	var cachedClearTimeout;
	
	function defaultSetTimout() {
	    throw new Error('setTimeout has not been defined');
	}
	function defaultClearTimeout () {
	    throw new Error('clearTimeout has not been defined');
	}
	(function () {
	    try {
	        if (typeof setTimeout === 'function') {
	            cachedSetTimeout = setTimeout;
	        } else {
	            cachedSetTimeout = defaultSetTimout;
	        }
	    } catch (e) {
	        cachedSetTimeout = defaultSetTimout;
	    }
	    try {
	        if (typeof clearTimeout === 'function') {
	            cachedClearTimeout = clearTimeout;
	        } else {
	            cachedClearTimeout = defaultClearTimeout;
	        }
	    } catch (e) {
	        cachedClearTimeout = defaultClearTimeout;
	    }
	} ())
	function runTimeout(fun) {
	    if (cachedSetTimeout === setTimeout) {
	        //normal enviroments in sane situations
	        return setTimeout(fun, 0);
	    }
	    // if setTimeout wasn't available but was latter defined
	    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
	        cachedSetTimeout = setTimeout;
	        return setTimeout(fun, 0);
	    }
	    try {
	        // when when somebody has screwed with setTimeout but no I.E. maddness
	        return cachedSetTimeout(fun, 0);
	    } catch(e){
	        try {
	            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
	            return cachedSetTimeout.call(null, fun, 0);
	        } catch(e){
	            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
	            return cachedSetTimeout.call(this, fun, 0);
	        }
	    }
	
	
	}
	function runClearTimeout(marker) {
	    if (cachedClearTimeout === clearTimeout) {
	        //normal enviroments in sane situations
	        return clearTimeout(marker);
	    }
	    // if clearTimeout wasn't available but was latter defined
	    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
	        cachedClearTimeout = clearTimeout;
	        return clearTimeout(marker);
	    }
	    try {
	        // when when somebody has screwed with setTimeout but no I.E. maddness
	        return cachedClearTimeout(marker);
	    } catch (e){
	        try {
	            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
	            return cachedClearTimeout.call(null, marker);
	        } catch (e){
	            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
	            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
	            return cachedClearTimeout.call(this, marker);
	        }
	    }
	
	
	
	}
	var queue = [];
	var draining = false;
	var currentQueue;
	var queueIndex = -1;
	
	function cleanUpNextTick() {
	    if (!draining || !currentQueue) {
	        return;
	    }
	    draining = false;
	    if (currentQueue.length) {
	        queue = currentQueue.concat(queue);
	    } else {
	        queueIndex = -1;
	    }
	    if (queue.length) {
	        drainQueue();
	    }
	}
	
	function drainQueue() {
	    if (draining) {
	        return;
	    }
	    var timeout = runTimeout(cleanUpNextTick);
	    draining = true;
	
	    var len = queue.length;
	    while(len) {
	        currentQueue = queue;
	        queue = [];
	        while (++queueIndex < len) {
	            if (currentQueue) {
	                currentQueue[queueIndex].run();
	            }
	        }
	        queueIndex = -1;
	        len = queue.length;
	    }
	    currentQueue = null;
	    draining = false;
	    runClearTimeout(timeout);
	}
	
	process.nextTick = function (fun) {
	    var args = new Array(arguments.length - 1);
	    if (arguments.length > 1) {
	        for (var i = 1; i < arguments.length; i++) {
	            args[i - 1] = arguments[i];
	        }
	    }
	    queue.push(new Item(fun, args));
	    if (queue.length === 1 && !draining) {
	        runTimeout(drainQueue);
	    }
	};
	
	// v8 likes predictible objects
	function Item(fun, array) {
	    this.fun = fun;
	    this.array = array;
	}
	Item.prototype.run = function () {
	    this.fun.apply(null, this.array);
	};
	process.title = 'browser';
	process.browser = true;
	process.env = {};
	process.argv = [];
	process.version = ''; // empty string to avoid regexp issues
	process.versions = {};
	
	function noop() {}
	
	process.on = noop;
	process.addListener = noop;
	process.once = noop;
	process.off = noop;
	process.removeListener = noop;
	process.removeAllListeners = noop;
	process.emit = noop;
	process.prependListener = noop;
	process.prependOnceListener = noop;
	
	process.listeners = function (name) { return [] }
	
	process.binding = function (name) {
	    throw new Error('process.binding is not supported');
	};
	
	process.cwd = function () { return '/' };
	process.chdir = function (dir) {
	    throw new Error('process.chdir is not supported');
	};
	process.umask = function() { return 0; };


/***/ }),

/***/ 3:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_3__;

/***/ }),

/***/ 4:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_4__;

/***/ }),

/***/ 5:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_5__;

/***/ }),

/***/ 24:
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(process) {'use strict';
	
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	
	var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _react = __webpack_require__(3);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _propTypes = __webpack_require__(4);
	
	var _propTypes2 = _interopRequireDefault(_propTypes);
	
	var _classnames = __webpack_require__(5);
	
	var _classnames2 = _interopRequireDefault(_classnames);
	
	var _Image = __webpack_require__(25);
	
	var _Image2 = _interopRequireDefault(_Image);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var Image = function (_Component) {
		_inherits(Image, _Component);
	
		function Image(props) {
			_classCallCheck(this, Image);
	
			var _this = _possibleConstructorReturn(this, (Image.__proto__ || Object.getPrototypeOf(Image)).call(this, props));
	
			_this.handleImageLoad = function () {
				// TODO: Chrome doesn't seem to update this onLoad.
				_this.setState({
					isLoaded: true
				});
			};
	
			_this.handleImageError = function (event) {
				event.target.src = '';
				_this.setState({
					isImageError: true
				});
			};
	
			_this.state = {
				isLoaded: false,
				isImageError: !_this.props.src ? true : false
			};
			return _this;
		}
	
		_createClass(Image, [{
			key: 'componentDidMount',
			value: function componentDidMount() {
				// Client side script may load after image loads, therefore it won't trigger
				// handleImageLoad, so we gotta add a check here.
				// http://stackoverflow.com/questions/39777833/image-onload-event-in-isomorphic-react-register-event-after-image-is-loaded
				if (this.props.src && this.node.complete) {
					this.handleImageLoad();
				}
			}
		}, {
			key: 'componentWillReceiveProps',
			value: function componentWillReceiveProps(nextProps) {
				if (this.props.src !== nextProps.src) {
					this.setState({
						isLoaded: false,
						isImageError: !nextProps.src ? true : false
					});
				}
			}
		}, {
			key: 'render',
			value: function render() {
				var _this2 = this;
	
				var imageStyle = _extends({
					paddingTop: this.props.aspectRatio && 100 / this.props.aspectRatio + '%'
				}, this.props.style);
	
				var pictureSource = this.props.sources && this.props.sources.map(function (source, i) {
					return _react2.default.createElement('source', {
						key: 'pictureSource-' + i,
						media: source.media,
						srcSet: source.srcSet
					});
				});
				var image = _react2.default.createElement('img', {
					src: this.props.src,
					alt: this.props.alt,
					onClick: this.props.onClick,
					onLoad: this.handleImageLoad,
					onError: this.props.isHandleImageError && this.handleImageError,
					width: this.props.width,
					height: this.props.height,
					srcSet: this.props.srcSet,
					sizes: this.props.sizes,
					ref: function ref(node) {
						return _this2.node = node;
					}
				});
	
				return _react2.default.createElement(
					'div',
					{
						className: (0, _classnames2.default)("maas-image", this.props.className, {
							"is-loaded": this.state.isLoaded,
							"is-loading": !this.state.isLoaded,
							"is-image-error": this.state.isImageError,
							"has-aspect-ratio": this.props.aspectRatio
						}),
						style: imageStyle,
						role: this.props.role,
						tabIndex: this.props.tabIndex
					},
					this.props.src && (pictureSource ? _react2.default.createElement(
						'picture',
						null,
						pictureSource,
						image
					) : image),
					this.state.isImageError && _react2.default.createElement(
						'div',
						{ className: 'maas-image__status' },
						this.props.noImageContent
					),
					this.props.showLoader && !this.state.isLoaded && !this.state.isImageError && _react2.default.createElement(
						'div',
						{ className: 'maas-image__status' },
						this.props.loadingContent
					)
				);
			}
		}]);
	
		return Image;
	}(_react.Component);
	
	Image.defaultProps = {
		showLoader: false,
		noImageContent: _react2.default.createElement('i', { className: 'fa fa-picture-o' }),
		loadingContent: _react2.default.createElement('i', { className: 'fa fa-circle-o-notch fa-spin' }),
		isHandleImageError: true
	};
	process.env.NODE_ENV !== "production" ? Image.propTypes = {
		className: _propTypes2.default.string,
		src: _propTypes2.default.string,
		alt: _propTypes2.default.string,
		width: _propTypes2.default.number,
		height: _propTypes2.default.number,
		aspectRatio: _propTypes2.default.number, // Width / Height
		srcSet: _propTypes2.default.string,
		sizes: _propTypes2.default.string,
		showLoader: _propTypes2.default.bool,
		// sources: PropTypes.array(PropTypes.shape({
		// 	media: PropTypes.string,
		// 	srcSet: PropTypes.string,
		// })),
		noImageContent: _propTypes2.default.object || _propTypes2.default.string,
		loadingContent: _propTypes2.default.object || _propTypes2.default.string,
		onClick: _propTypes2.default.func,
		isHandleImageError: _propTypes2.default.bool
	} : void 0;
	exports.default = Image;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),

/***/ 25:
/***/ (function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ })

/******/ })
});
;
//# sourceMappingURL=Image.js.map