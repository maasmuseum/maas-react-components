(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("prop-types"));
	else if(typeof define === 'function' && define.amd)
		define(["prop-types"], factory);
	else if(typeof exports === 'object')
		exports["maas-react-components"] = factory(require("prop-types"));
	else
		root["maas-react-components"] = factory(root["PropTypes"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_4__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(6);


/***/ }),
/* 1 */,
/* 2 */,
/* 3 */,
/* 4 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_4__;

/***/ }),
/* 5 */,
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.IdPropTypes = exports.MenuItemsPropTypes = exports.MenuItemPropTypes = undefined;
	
	var _propTypes = __webpack_require__(4);
	
	var _propTypes2 = _interopRequireDefault(_propTypes);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	// Menu items should have this shape
	var MenuItemPropTypes = exports.MenuItemPropTypes = _propTypes2.default.shape({
	  name: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.object]).isRequired,
	  url: _propTypes2.default.string.isRequired,
	  isHtmlLink: _propTypes2.default.bool,
	  isActive: _propTypes2.default.bool,
	  ariaLabel: _propTypes2.default.string
	});
	
	// An array of MenuItems
	var MenuItemsPropTypes = exports.MenuItemsPropTypes = _propTypes2.default.arrayOf(MenuItemPropTypes);
	
	// IDs can come from Emu (number) or MongoId, so need to account for it
	var IdPropTypes = exports.IdPropTypes = _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]);

/***/ })
/******/ ])
});
;
//# sourceMappingURL=propTypes.js.map