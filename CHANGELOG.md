## MAAS React Components Change Log

All notable changes to this project will be documented in this file.

### [v1.3.0] - 2017-07-07
- Added classNames as external in Webpack. Added Image component.

### [v1.2.0] - 2017-07-06
- Added some new components (WIP), enabled some image loaders in Webpack.

### [v1.1.1] - 2017-07-05
- Got externals working in Webpack, preventing peer deps from being bundled.

### [v1.0.0] - 2017-06-30
- Just updated this changelog.
