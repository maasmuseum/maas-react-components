import * as path from 'path';

import webpack from 'webpack';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import SystemBellPlugin from 'system-bell-webpack-plugin';
import CleanWebpackPlugin from 'clean-webpack-plugin';
import merge from 'webpack-merge';

const pkg = require('./package.json');

const TARGET = process.env.npm_lifecycle_event || '';
const ROOT_PATH = __dirname;

// Update this whenever there are new modules/components
const distModules = {
  index: './src/index.js',
  Link: ['./src/Link'],
  Menu: ['./src/Menu'],
  Image: ['./src/Image'],
  propTypes: ['./src/propTypes.js'],
  Header: ['./src/Header'],
  Footer: ['./src/Footer'],
  MegaMenu: ['./src/MegaMenu'],
  Video: ['./src/Video'],
  Map: ['./src/Map'],
};

// Add any external dependencies that you don't want to be bundled up inside components.
const externals = {
  react: {
    root: 'React',
    commonjs: 'react',
    commonjs2: 'react',
    amd: 'React',
  },
  'react-router': {
    root: 'ReactRouter',
    commonjs2: 'react-router',
    commonjs: 'react-router',
    amd: 'react-router',
  },
  'prop-types': {
    root: 'PropTypes',
    commonjs2: 'prop-types',
    commonjs: 'prop-types',
    amd: 'prop-types',
  },
  'classnames': {
    root: 'classNames',
    commonjs2: 'classnames',
    commonjs: 'classnames',
    amd: 'classnames',
  },
};

const config = {
  paths: {
    dist: path.join(ROOT_PATH, 'dist'),
    src: path.join(ROOT_PATH, 'src'),
    // src: {
    //   index: './src/index.js',
    //   Header: ['./src/Header'],
    // },
    docs: path.join(ROOT_PATH, 'docs')
  },
  filename: 'index',
  library: 'maas-react-components'
};

const postcssConfig = [
  require('postcss-import'),
  require('precss')(),
  require('lost'),
  require('autoprefixer')({
    browsers: [
      '>1%',
      'last 4 versions',
      'Firefox ESR',
      'not ie < 9', // React doesn't support IE8 anyway
    ],
  }),
];

const isDebug = true;

process.env.BABEL_ENV = TARGET;

const common = {
  resolve: {
    extensions: ['', '.js', '.jsx', '.css', '.png', '.jpg', 'scss']
  },
  module: {
    preLoaders: [
      {
        test: /\.jsx?$/,
        loaders: ['eslint'],
        include: [
          config.paths.docs,
          config.paths.src
        ]
      }
    ],
    loaders: [
      {
        test: /\.md$/,
        loaders: ['catalog/lib/loader', 'raw']
      },
      // {
      //   test: /\.png$/,
      //   loader: 'url?limit=100000&mimetype=image/png',
      //   include: config.paths.docs
      // },
      {
        test: /\.(ico|jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2)(\?.*)?$/,
        loader: 'file',
        include: config.paths.src
      },
      {
        test: /\.json$/,
        loader: 'json',
        include: path.join(ROOT_PATH, 'package.json')
      },
      {
        test: /\.scss$/,
        loaders: [
          'style-loader',
          'css-loader',
          'postcss?pack=default'
        ]
        // loader: ExtractTextPlugin.extract(
        //   'style', // The backup style loader
        //   'css?sourceMap!postcss?pack=default'
        // )
      },
    ]
  },
  plugins: [
    new SystemBellPlugin(),
    // new ExtractTextPlugin('components.css'),
  ],

  // The list of plugins for PostCSS
  // https://github.com/postcss/postcss
  postcss() {
    return {
      default: [
        ...postcssConfig
      ],
    };
  },

};

const siteCommon = {
  plugins: [
    new HtmlWebpackPlugin({
      template: require('html-webpack-template'), // eslint-disable-line global-require
      inject: false,
      mobile: true,
      title: pkg.name,
      appMountId: 'app'
    }),
    new webpack.DefinePlugin({
      NAME: JSON.stringify(pkg.name),
      USER: JSON.stringify(pkg.user),
      VERSION: JSON.stringify(pkg.version)
    })
  ]
};

if (TARGET === 'start') {
  module.exports = merge(common, siteCommon, {
    devtool: 'eval-source-map',
    entry: {
      docs: [config.paths.docs]
    },
    plugins: [
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': '"development"'
      }),
      new webpack.HotModuleReplacementPlugin(),
    ],
    module: {
      loaders: [
        {
          test: /\.css$/,
          loaders: ['style', 'css']
        },
        // {
        //   test: /\.scss/,
        //   loaders: [
        //     // 'isomorphic-style-loader',
        //     `css-loader?${JSON.stringify({
        //       // CSS Loader https://github.com/webpack/css-loader
        //       importLoaders: 1,
        //       sourceMap: isDebug,
        //       // CSS Modules https://github.com/css-modules/css-modules
        //       // modules: true,
        //       localIdentName: isDebug ? '[name]-[local]-[hash:base64:5]' : '[hash:base64:5]',
        //       // CSS Nano http://cssnano.co/options/
        //       minimize: !isDebug,
        //       discardComments: { removeAll: true },
        //     })}`,
        //     'postcss-loader?pack=default',
        //     // 'postcss-loader',
        //   ],
        // },
        {
          test: /\.jsx?$/,
          loaders: ['babel?cacheDirectory'],
          include: [
            config.paths.docs,
            config.paths.src
          ]
        }
      ]
    },

    devServer: {
      historyApiFallback: true,
      hot: true,
      inline: true,
      progress: true,
      host: process.env.HOST,
      port: process.env.PORT,
      stats: 'errors-only'
    }
  });
}

const distCommon = {
  devtool: 'source-map',
  output: {
    path: config.paths.dist,
    libraryTarget: 'umd',
    library: config.library
  },
  entry: distModules,
  externals: externals,
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        loaders: ['babel'],
        include: config.paths.src
      },
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract(
          'style', // The backup style loader
          'css?sourceMap!postcss?pack=default'
        )
      },
      // {
      //   test: /\.png$/,
      //   loader: 'url?limit=100000&mimetype=image/png',
      //   include: config.paths.src
      // },
      {
        test: /\.(ico|jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2)(\?.*)?$/,
        loader: 'file',
        include: config.paths.src
      },
    ]
  },
  plugins: [
    new SystemBellPlugin(),
    new ExtractTextPlugin('[name].css'),
  ],

  // The list of plugins for PostCSS
  // https://github.com/postcss/postcss
  postcss() {
    return {
      default: [
        ...postcssConfig,
      ],
    };
  },

};

if (TARGET === 'dist') {
  module.exports = merge(distCommon, {
    output: {
      filename: `[name].js`
    }
  });
}

if (TARGET === 'dist:min') {
  module.exports = merge(distCommon, {
    output: {
      filename: `[name].min.js`
    },
    plugins: [
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false
        }
      })
    ]
  });
}

if (!TARGET) {
  module.exports = common;
}

if (TARGET === 'gh-pages' || TARGET === 'gh-pages:stats') {
  module.exports = merge(common, siteCommon, {
    entry: {
      app: config.paths.docs,
      vendors: [
        'react',
        'react-dom'
      ]
    },
    output: {
      path: './gh-pages',
      filename: '[name].[chunkhash].js',
      chunkFilename: '[chunkhash].js'
    },
    plugins: [
      new CleanWebpackPlugin(['gh-pages'], {
        verbose: false
      }),
      new ExtractTextPlugin('[name].[chunkhash].css'),
      new webpack.DefinePlugin({
          // This affects the react lib size
        'process.env.NODE_ENV': '"production"'
      }),
      new webpack.optimize.DedupePlugin(),
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false
        }
      }),
      new webpack.optimize.CommonsChunkPlugin(
        'vendor',
        '[name].[chunkhash].js'
      )
    ],
    module: {
      loaders: [
        {
          test: /\.css$/,
          loader: ExtractTextPlugin.extract('style', 'css')
        },
        {
          test: /\.jsx?$/,
          loaders: ['babel'],
          include: [
            config.paths.docs,
            config.paths.src
          ]
        }
      ]
    }
  });
}
