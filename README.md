# MAAS React Components
A global library of React components for MAAS websites and apps. Currently in use on collection.maas.museum and new.maas.museum.

| Component | Description |
| -- | -- |
| Link | Component that wraps React Router (v3) Link |
| Menu | Renders unordered list for navigation purposes |
| Image | Wrapper for img tag, with loading animation and other features |
| Header | WIP |
| Footer | WIP |
| MegaMenu | WIP |
| Map | WIP |
| Video | WIP |

## Getting Started
`npm install` will get you going.

### Creating New Components
Add your new component into `/src`. Use one of the existing ones as reference. This package is set up for `.scss` files to be used with your components.

#### Webpack
New components and files need to be added to `webpack.config.babel.js`.

```js
const distModules = {
  index: './src/index.js',
  Header: ['./src/Header'],
  Menu: ['./src/Menu'],
  propTypes: ['./src/propTypes.js'],
  ...
  // Add your new component or file
};
```

##### Externals
To ensure external dependencies aren't bundled up inside components, increasing the bundle size and duplicating code, add them to `webpack.config.babel.js`.

```js
const externals = {
  react: {
    commonjs: 'react',
    commonjs2: 'react',
    amd: 'React',
    root: 'React'
  },
  ...
  // Add dependency
};
```

### Build Step
Run the following to build a new version of the package.
```bash

# Update git repo
$ git add . && git commit -am "Component update" && git push
# Version package (base on Semver)
$ npm version patch|minor|major
# Push commit and tags
$ git push && git push --tags
```

### Development
Because these components are isolated, it can be difficult to develop. You have two options:

#### 1. Local Development Server
Run `npm start` and go to `localhost:8080`. Import/update your component in `/docs/index.jsx` and see your changes.

#### 2. Within Your Project
The following commands can be used while working in this package AND your project (eg. collection or new-museum).

```bash
# In /maas-react-components
$ npm link
# Navigate to your project
$ cd ../your-project
# Link maas-react-components within your project. Make sure it is already in package.json in your project.
$ npm link maas-react-components
# This symlinks maas-react-components to your project + other voodoo stuff.
# Go back to maas-react-components
$ cd ../maas-react-components
# Make some edits and build a dist
$ npm run dist
# Refresh and changes should show up in your project server
```

##### Styles
Within your project, styles have to imported separately to React components. In `collection` and `new-museum`, add component css to `src/styles/components.scss`.

```css
/* In src/styles/components.scss */
@import 'maas-react-components/dist/Menu.css';
@import 'maas-react-components/dist/NewComponent.css';
```

## TODOS
- [ ] Get classnames in Webpack externals config working
- [ ] Import images properly within component

---

## Based on react-component-boilerplate
The following docs are from `react-component-boilerplate`.

Original repo:
`https://github.com/survivejs/react-component-boilerplate `

For more info:
> Check out [SurviveJS - Webpack and React](http://survivejs.com/) to dig deeper into the topic.

### Common Tasks

* Developing - **npm start** - Runs the development server at *localhost:8080* and use Hot Module Replacement. You can override the default host and port through env (`HOST`, `PORT`).
* Creating a version - **npm version <x.y.z>** - Updates */dist* and *package.json* with the new version and create a version tag to Git.
* Publishing a version - **npm publish** - Pushes a new version to npm and updates the project site.

### Testing

The test setup is based on Jest. Code coverage report is generated to `coverage/`. The coverage information is also uploaded to codecov.io after a successful Travis build.

* Running tests once - **npm test**
* Running tests continuously - **npm run test:watch**
* Running individual tests - **npm test -- <pattern>** - Works with `test:watch` too.
* Linting - **npm run test:lint** - Runs ESLint.

### Documentation Site

The boilerplate includes a [GitHub Pages](https://pages.github.com/) specific portion for setting up a documentation site for the component. The main commands handle with the details for you. Sometimes you might want to generate and deploy it by hand, or just investigate the generated bundle.

* Building - **npm run gh-pages** - Builds the documentation into `./gh-pages` directory.
* Deploying - **npm run deploy-gh-pages** - Deploys the contents of `./gh-pages` to the `gh-pages` branch. GitHub will pick this up automatically. Your site will be available through *<user name>.github.io/<project name>`.
* Generating stats - **npm run stats** - Generates stats that can be passed to [webpack analyse tool](https://webpack.github.io/analyse/). This is useful for investigating what the build consists of.



### License

*react-component-boilerplate* is available under MIT. See LICENSE for more details.
